---
layout: page
title: Acerca de
permalink: /about/
---


Recuerda que puedes **contactar** con nosotros de las siguientes formas:  

+ Twitter: <https://twitter.com/llegamostarde_>
+ Instagram: <https://www.instagram.com/llegamostardepodcast/>
+ Correo: <llegamostardeyt@gmail.com>
+ Web: <https://llegamostarde.gitlab.io/>
+ Feed Podcast: <https://llegamostarde.gitlab.io/feed>
+ Spotify: <https://open.spotify.com/show/5DtaARdlYwzhjjkuxJ59z7>