---
layout: post  
title: "#8 - Samurai Champloo"  
date: 2019-02-23  
categories: podcast  
image: images/miniatura-podcast-8.png  
podcast_link: https://archive.org/download/8SamuraiChamploo/%238%20-%20Samurai%20Champloo
tags: [anime, samurai champloo, opinión, debate, podcast, chile]  
comments: true 
---
Llega el momento de hablar de un anime, viajamos a la época Edo de Japón para hablar de hip-hop y samurais.


<audio controls>
  <source src="https://archive.org/download/8SamuraiChamploo/%238%20-%20Samurai%20Champloo.mp3">
  <source src="">
</audio>


<iframe width="560" height="315" src="https://www.youtube.com/embed/KInXV46pOaQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/llegamostarde_>
+ Correo: <llegamostardeyt@gmail.com.com>
+ Web: <https://llegamostarde.gitlab.io//>
+ Youtube: <https://www.youtube.com/channel/UCXsdwLDyCKk0cTkYhIblnQQ>
+ Feed Podcast: <https://llegamostarde.gitlab.io/feed>