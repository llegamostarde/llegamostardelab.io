---
layout: post  
title: "#5 - Alfred Hitchcock"  
date: 2019-02-02  
categories: podcast  
image: images/miniatura-podcast-5.png  
podcast_link: https://archive.org/download/5AlfredHitchcock/%235%20-%20Alfred%20Hitchcock
tags: [cine, hitchcock, suspenso, podcast, opinión, debate, Llegamos Tarde]  
comments: true 
---
Toca hablar de un director importante para la historia del cine, alguien que creó tendencias por su estilo. Nos terminamos decantando por Alfred Hitchcock, el maestro del suspenso. Revisamos 3 de sus películas, 3 a modo de introducirnos en la filmografía de este director tan importante.



<audio controls>
  <source src="https://archive.org/download/5AlfredHitchcock/%235%20-%20Alfred%20Hitchcock.mp3">
  <source src="">
</audio>


<iframe width="560" height="315" src="https://www.youtube.com/embed/-487oCzrdjc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/llegamostarde_>
+ Correo: <llegamostardeyt@gmail.com.com>
+ Web: <https://llegamostarde.gitlab.io//>
+ Youtube: <https://www.youtube.com/channel/UCXsdwLDyCKk0cTkYhIblnQQ>
+ Feed Podcast: <https://llegamostarde.gitlab.io/feed>