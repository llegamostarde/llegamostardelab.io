---
layout: post  
title: "#1 - Katsuhiro Otomo"  
date: 2018-12-31  
categories: podcast  
image: https://ia801407.us.archive.org/3/items/LlegamosTarde1KatsuhiroOtomo/__ia_thumb.jpg
podcast_link: https://archive.org/download/LlegamosTarde1KatsuhiroOtomo/%231%20Katsuhiro%20Otomo
tags: [audio, podcast, Llegamos Tarde]  
comments: true 
---
En esta ocasión, como primer capítulo del podcast y que iniciaremos la serie mensual dedicada a autores importantes, hemos decidido comenzar con el anime, y finalmente nos decantamos por el director, escritor y mangaka Katsuhiro Otomo, autor de Akira, Metropolis, Steamboy, entre otras. Esperamos que les guste nuestro primer capítulo.

<audio controls>
  <source src="https://archive.org/download/LlegamosTarde1KatsuhiroOtomo/%231%20Katsuhiro%20Otomo.mp3">
  <source src="">
</audio>



<iframe width="560" height="315" src="https://www.youtube.com/embed/w65MXavhkoA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/llegamostarde_>
+ Correo: <llegamostardeyt@gmail.com.com>
+ Web: <https://llegamostarde.gitlab.io//>
+ Youtube: <https://www.youtube.com/channel/UCXsdwLDyCKk0cTkYhIblnQQ>
+ Feed Podcast: <https://llegamostarde.gitlab.io/feed>

