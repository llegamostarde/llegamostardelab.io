---
layout: post  
title: "#2 - Roma / Isla de Perros"  
date: 2019-01-05  
categories: podcast  
featimage: images/miniatura_podcast_2.png  
podcast_link: https://archive.org/download/RomaIslaDePerros/%232%20Roma%20%3A%20Isla%20de%20Perros
tags: [cine, roma, isleofdogs, cuaron, podcast, Llegamos Tarde]  
comments: true 
---
En este capítulo comentamos 2 películas que se destacaron el año 2018, no son necesariamente las mejores, pero quisimos darles (más) espacio.


<audio controls>
  <source src="https://archive.org/download/RomaIslaDePerros/%232%20Roma%20%3A%20Isla%20de%20Perros.mp3">
  <source src="">
</audio>


<iframe width="560" height="315" src="https://www.youtube.com/embed/elH89mvZ3hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/llegamostarde_>
+ Correo: <llegamostardeyt@gmail.com.com>
+ Web: <https://llegamostarde.gitlab.io//>
+ Youtube: <https://www.youtube.com/channel/UCXsdwLDyCKk0cTkYhIblnQQ>
+ Feed Podcast: <https://llegamostarde.gitlab.io/feed>
