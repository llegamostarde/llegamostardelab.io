---
layout: post  
title: "#9 - The Mandalorian"  
date: 2020-01-27  
categories: podcast  
image: images/miniatura-podcast-8.png  
podcast_link: https://archive.org/download/the-mandalorian/the-mandalorian.mp3
tags: [podcast, chile, opinión, star wars, baby yoda, the child, the mandalorian]  
comments: true 
---
La serie que dejó chica a la última trilogía de Star Wars, hoy toca hablar sobre The Mandalorian.


<audio controls>
  <source src="https://archive.org/download/8SamuraiChamploo/%238%20-%20Samurai%20Champloo.mp3">
  <source src="">
</audio>


<iframe width="560" height="315" src="https://youtu.be/PJIg-pqvvWY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Recuerda que puedes **contactar** con nosotros de las siguientes formas:

+ Twitter: <https://twitter.com/llegamostarde_>
+ Correo: <llegamostardeyt@gmail.com.com>
+ Web: <https://llegamostarde.gitlab.io//>
+ Youtube: <https://www.youtube.com/channel/UCXsdwLDyCKk0cTkYhIblnQQ>
+ Feed Podcast: <https://llegamostarde.gitlab.io/feed>